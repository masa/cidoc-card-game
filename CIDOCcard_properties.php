<?php
/*********************************************************
Auteur : Olivier Marlet
Contact : olivier.marlet@univ-tours.fr
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date première version : 19/12/2023
Date dernière mise à jour : 08/11/2024
Objectif : Créer automatiquement un fichier PDF avec les cartes des propriétés à partir du fichier RDF de l'ontologie du CIDOC CRM.
Versions logiciels : PHP 8.2.13, librairie FPDF 1.86
*********************************************************/

// #### PARAMETRES ####

// éditer le fichier parameters.php pour indiquer vos préférences
include("CIDOCcard_parameters.php");

// FORMAT page
$mapage =array($orientationp, $format);
switch($mapage){
	case array("Portrait","A4") :      $pageh = 297;   $pagel = 210;   break;
	case array("Landscape","A4") :     $pageh = 210;   $pagel = 297;   break;
	case array("Portrait","Letter") :  $pageh = 279.4; $pagel = 215.9; break;
	case array("Landscape","Letter") : $pageh = 215.9; $pagel = 279.4; break;
	default : $pageh = 297;   $pagel = 210;
}

// #### FONCTIONS ####

// tableau des couleurs à partir des branches
function lire_couleur($clr_entities){
	// Couleur et icones associées aux branches
	include("CIDOCcard_colors.php");
	// exploitation du fichier de référence des branches
	$xml = new XMLReader();
    $xml->open($clr_entities, "r");
	while ($xml->read()) {
		if($xml->nodeType == XMLReader::END_ELEMENT) {
			continue;
		}
		if($xml->name == "entity"){
			// pour chaque entité...
			$entity = $xml->getAttribute('code');
			// ... on récupère les branches associées
			$branches = $xml->getAttribute('branch');
			$brchtab = explode(",",$branches);
			// initialisation des informations
			$clr1="";$clr2="";$clr3="";
			// pour chaque branche :
			for($i=0;$i<count($brchtab);$i++){
				$num = $i+1;
				// récupération de la couleur
				${"clr$num"} = $style[$brchtab[$i]][0];
				$tabclrcla["$entity"] = array(
					"color1"=>"$clr1",
					"color2"=>"$clr2",
					"color3"=>"$clr3",
				);
			}
		}
	}
	return $tabclrcla;
}

// mise en page
$nbrecol = floor(($pagel-$pagemd-$pagemg)/$cartelp); // nbre de cartes par colonne
$margel  = ($pagel-($nbrecol*$cartelp))/2; // marges latérales (de chaque côté)
$nbrelig = floor(($pageh-$pagemh-$pagemb)/$cartehp); // nbre de cartes par ligne
$margeh  = ($pageh-($nbrelig*$cartehp))/2; // marges longitudinales (en haut et en bas)
$parpage = $nbrecol*$nbrelig; // nombre de cartes par page

require('fpdf/tfpdf.php');

class RPDF extends tFPDF {
	function TextWithDirection($x, $y, $txt, $direction='R') {
		if ($direction=='R')
			$s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',1,0,0,1,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
		elseif ($direction=='L')
			$s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',-1,0,0,-1,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
		elseif ($direction=='U')
			$s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',0,1,-1,0,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
		elseif ($direction=='D')
			$s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',0,-1,1,0,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
		else
			$s=sprintf('BT %.2F %.2F Td (%s) Tj ET',$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
		if ($this->ColorFlag)
			$s='q '.$this->TextColor.' '.$s.' Q';
		$this->_out($s);
	}
	function TextWithRotation($x, $y, $txt, $txt_angle, $font_angle=0) {
		$font_angle+=90+$txt_angle;
		$txt_angle*=M_PI/180;
		$font_angle*=M_PI/180;

		$txt_dx=cos($txt_angle);
		$txt_dy=sin($txt_angle);
		$font_dx=cos($font_angle);
		$font_dy=sin($font_angle);

		$s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',$txt_dx,$txt_dy,$font_dx,$font_dy,$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
		if ($this->ColorFlag)
			$s='q '.$this->TextColor.' '.$s.' Q';
		$this->_out($s);
	}
}

class PDF_Polygon extends RPDF {
	function Polygon($points, $style='D') {
		//Draw a polygon
		if($style=='F')
			$op = 'f';
		elseif($style=='FD' || $style=='DF')
			$op = 'b';
		else
			$op = 's';

		$h = $this->h;
		$k = $this->k;

		$points_string = '';
		for($i=0; $i<count($points); $i+=2){
			$points_string .= sprintf('%.2F %.2F', $points[$i]*$k, ($h-$points[$i+1])*$k);
			if($i==0)
				$points_string .= ' m ';
			else
				$points_string .= ' l ';
		}
		$this->_out($points_string . $op);
	}
}

$pdf = new PDF_Polygon();

// #### TABLEAUX DE DONNÉES ####

// tableau des relations hiérarchiques pour récupérer les sous-propriétés d'une propriété
function IsA($file){
    $xml = new XMLReader();
    $xml->open($file, "r");
	$isa = array();
	while($xml->read()) {
		// récupération des infos d'une entité
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdf:Property"){
			$property = array();
			$prop = $xml->getAttribute('rdf:about');
		}
		// récupération des super-classes
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:subPropertyOf"){
			$parent = $xml->getAttribute('rdf:resource');
			$isa[$parent][] = $prop;
		}
	}
	return $isa;
	$xml->XMLReader::close;
}

// tableau contenant les informations de chaque propriété
function lire_prop($file,$color){
	// Lecture du fichier RDF
    $xml = new XMLReader();
    $xml->open($file, "r");
	global $langue;
	$properties = array(); // tableau général
	while($xml->read()) {
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdf:Property"){
			$property = array(); // tableau des infos d'une propriété
			$prop = $xml->getAttribute('rdf:about');
			$codep = substr($prop, 0, strpos($prop, "_"));
		}
		if(isset($codep) && strpos($codep, "i")===FALSE) {
			// récupération du nom de l'entité en fonction de la langue demandée
			if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:label" && $xml->getAttribute('xml:lang')=="$langue"){
				$property["propname"] = $xml->readString();
			}
			// récupération de la description
			if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:comment"){
				$property["comment"] = $xml->readString();
			}
			// récupération des super-propriétés
			if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:subPropertyOf"){
				$property["super"][] = $xml->getAttribute('rdf:resource');
			}
			// récupération du domaine
			if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:domain"){
				$property["domain"] = $xml->getAttribute('rdf:resource');
				// gestion des branches : récupération des couleurs
				$numd = substr($property["domain"],0,strpos($property["domain"],"_"));
				$property["dclr1"] = $color["$numd"]["color1"];
				$property["dclr2"] = $color["$numd"]["color2"];
				$property["dclr3"] = $color["$numd"]["color3"];
			}
			// récupération du co-domaine
			if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:range"){
				$property["range"] = $xml->getAttribute('rdf:resource');
				$numr = substr($property["range"],0,strpos($property["range"],"_"));
				$property["rclr1"] = $color["$numr"]["color1"];
				$property["rclr2"] = $color["$numr"]["color2"];
				$property["rclr3"] = $color["$numr"]["color3"];
			}
			// ajout des infos de l'entité au tableau général
			if ($xml->nodeType == XMLReader::END_ELEMENT && $xml->name == "rdf:Property"){
				$properties[$prop] = $property;
			}
		}
	}
	return $properties;
	$xml->XMLReader::close;
}

// tableau contenant les informations de chaque propriété inverse
function lire_propi($file){
    $xml = new XMLReader();
    $xml->open($file, "r");
	$ip = array(); // tableau général
	while($xml->read()) {
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdf:Property"){
			$prop = $xml->getAttribute('rdf:about');
			$codep = substr($prop, 0, strpos($prop, "_"));
			if(isset($codep) && strpos($codep, "i")!==FALSE) {
				$ip[$codep]="$prop";
			}
		}
	}
	return $ip;
	$xml->XMLReader::close;
}

// tableau des quantités souhaitées de chaque propriété
function lire_quantite($quantityfile){
	$fic = fopen($quantityfile, 'r');
	$quantity = array();
	for ($ligne = fgetcsv($fic, 1024, ";"); !feof($fic); $ligne = fgetcsv($fic, 1024, ";")) {
		$j = sizeof($ligne);
		$entnm = $ligne[0];
		$qtt   = $ligne[1];
		$quantity[$entnm]=$qtt;
	}
	return $quantity;
}

// Fusion du fichier CRM avec celui des Primitves Values
if($pvfile!=""){
    $doc1 = new DOMDocument();
    $doc1->load($file);

    $doc2 = new DOMDocument();
    $doc2->load($pvfile);

    // get 'rdf:RDF' element of document 1
    $res1 = $doc1->getElementsByTagName('RDF')->item(0);

    // iterate over 'rdfs:Class' elements of document 2
    $items2 = $doc2->getElementsByTagName('Class');
    for ($i = 0; $i < $items2->length; $i ++) {
        $item2 = $items2->item($i);

        // import/copy item from document 2 to document 1
        $item1 = $doc1->importNode($item2, true);

        // append imported item to document 1 'rdf:RDF' element
        $res1->appendChild($item1);
    }
    // iterate over 'rdf:Property' elements of document 2
    $props2 = $doc2->getElementsByTagName('Property');
    for ($i = 0; $i < $props2->length; $i ++) {
        $prop2 = $props2->item($i);

        // import/copy item from document 2 to document 1
        $prop1 = $doc1->importNode($prop2, true);

        // append imported item to document 1 'rdf:RDF' element
        $res1->appendChild($prop1);
    }
	$doc1->save('merged.xml');
	$file = "merged.xml";
}

// récupération de la version de l'ontologie
function version($codepropriete, $array){
	foreach($array as $key => $val){
		if($val["propriete"] === $codepropriete){
			return $key;
		}
	}
}

///////////////// ICI COMMENCE LE CONTENU POUR LA CREATION DU PDF /////////////////////

// récupération du tableau des relations IsA
$isa = isa($file);
// récupération des couleurs associées
$color= lire_couleur($clr_entities);
// stockage de l'ontologie dans un tableau associatif
$onto = lire_prop($file,$color);
// récupération des propriétés inverses
$propi = lire_propi($file);
// récupération du tableau des quantités
$quantity = lire_quantite($quantityfile);
// initialisation
$colonne=0;$ligne=0;$page=1;

$pdf->AddPage($orientationp, $format);
// Add a Unicode font (uses UTF-8)
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);

foreach($onto as $pid => $pprop){
	// Gestion de la quantité de cartes à créer pour chaque entité
	$qtt = $quantity[$pid];
	for($q=1; $q<=$qtt; $q++){
		// récupération de l'information depuis le tableau
		// boucle pour chaque propriété
		$codeprop    = substr($pid, 0, strpos($pid, "_"));
		$propname    = $onto[$pid]["propname"];
		$dclr1       = $onto[$pid]["dclr1"];
		$dclr2       = $onto[$pid]["dclr2"];
		$dclr3       = $onto[$pid]["dclr3"];
		$rclr1       = $onto[$pid]["rclr1"];
		$rclr2       = $onto[$pid]["rclr2"];
		$rclr3       = $onto[$pid]["rclr3"];
		$dclr1R      = base_convert(substr($dclr1,1,2),16,10);
		$dclr1G      = base_convert(substr($dclr1,3,2),16,10);
		$dclr1B      = base_convert(substr($dclr1,5,2),16,10);
		$dclr2R      = base_convert(substr($dclr2,1,2),16,10);
		$dclr2G      = base_convert(substr($dclr2,3,2),16,10);
		$dclr2B      = base_convert(substr($dclr2,5,2),16,10);
		$dclr3R      = base_convert(substr($dclr3,1,2),16,10);
		$dclr3G      = base_convert(substr($dclr3,3,2),16,10);
		$dclr3B      = base_convert(substr($dclr3,5,2),16,10);
		$rclr1R      = base_convert(substr($rclr1,1,2),16,10);
		$rclr1G      = base_convert(substr($rclr1,3,2),16,10);
		$rclr1B      = base_convert(substr($rclr1,5,2),16,10);
		$rclr2R      = base_convert(substr($rclr2,1,2),16,10);
		$rclr2G      = base_convert(substr($rclr2,3,2),16,10);
		$rclr2B      = base_convert(substr($rclr2,5,2),16,10);
		$rclr3R      = base_convert(substr($rclr3,1,2),16,10);
		$rclr3G      = base_convert(substr($rclr3,3,2),16,10);
		$rclr3B      = base_convert(substr($rclr3,5,2),16,10);
//		if(!isset($onto[$pid]["comment"])){$scopenote = "???";}
		$scopenote   = str_replace("\n"," ",$onto[$pid]["comment"]);
		$scopenote   = str_replace(array("&lt;","&gt;","&amp;"),array("<",">","&"),$scopenote);
		if(strlen($scopenote)>$txtmaxp){
			$scopenote = substr($scopenote,0,$txtmaxp);
			$scopenote = substr($scopenote,0,strrpos($scopenote," "))."...";
		}		
		$domain      = str_replace("_"," ",$onto[$pid]["domain"]);
		$range       = str_replace("_"," ",$onto[$pid]["range"]);
		$comm[] = array($pid, $scopenote);
		// ici écriture de la carte
		$posX = $margel+($colonne*$cartelp);
		$posY = $margeh+($ligne*$cartehp);
		// fond de la carte
		$pdf->SetFillColor(240,240,240);
		$pdf->Rect($posX, $posY, $cartelp, $cartehp, "F");
		// numéro d'entité
		$pdf->SetFont('Arial','B',12);
		$pdf->SetXY($posX+($cartelp/3),$posY+3.25);
		$pdf->SetTextColor(150,150,150);
		$pdf->SetFillColor(255,255,255);
		$pdf->Polygon(array(
			$posX+($cartelp/3),$posY+2,
			$posX+(2*($cartelp/3))-2.5,$posY+2,
			$posX+(2*($cartelp/3)),$posY+2+2.5,
			$posX+(2*($cartelp/3))-2.5,$posY+2+5,
			$posX+($cartelp/3),$posY+2+5,
			$posX+($cartelp/3)+2.5,$posY+2+2.5),
			'F');
		$pdf->Cell($cartelp/3, 3, $codeprop, 0, 1, "C", False);
		
		// couleur DOMAIN
		if($dclr3!=""){
			$pdf->SetFillColor($dclr3R,$dclr3G,$dclr3B);
			$pdf->Rect($posX, $posY+(($cartehp/3)*2), 4, $cartehp/3, "F");
			$pdf->SetFillColor($dclr2R,$dclr2G,$dclr2B);
			$pdf->Rect($posX, $posY+($cartehp/3), 4, $cartehp/3, "F");
			$pdf->SetFillColor($dclr1R,$dclr1G,$dclr1B);
			$pdf->Rect($posX, $posY, 4, $cartehp/3, "F");
		}elseif($dclr2!=""){
			$pdf->SetFillColor($dclr2R,$dclr2G,$dclr2B);
			$pdf->Rect($posX, $posY+($cartehp/2), 4, $cartehp/2, "F");
			$pdf->SetFillColor($dclr1R,$dclr1G,$dclr1B);
			$pdf->Rect($posX, $posY, 4, $cartehp/2, "F");
		}else{
			$pdf->SetFillColor($dclr1R,$dclr1G,$dclr1B);
			$pdf->Rect($posX, $posY, 4, $cartehp, "F");
		}
		$pdf->SetFont('Arial','B',8);
		$pdf->SetTextColor(255,255,255);
		$pdf->SetXY($posX+4,$posY+3.25);
		$pdf->Cell($cartelp/3, 3, "DOMAIN", 0, 1, "C", False);
		$pdf->SetXY($posX+(($cartelp/3)*2)-4,$posY+3.25);
		$pdf->Cell($cartelp/3, 3, "RANGE", 0, 1, "C", False);
		//couleur RANGE
		if($rclr3!=""){
			$pdf->SetFillColor($rclr3R,$rclr3G,$rclr3B);
			$pdf->Rect($posX+$cartelp-4, $posY+(($cartehp/3)*2), 4, $cartehp/3, "F");
			$pdf->SetFillColor($rclr2R,$rclr2G,$rclr2B);
			$pdf->Rect($posX+$cartelp-4, $posY+($cartehp/3), 4, $cartehp/3, "F");
			$pdf->SetFillColor($rclr1R,$rclr1G,$rclr1B);
			$pdf->Rect($posX+$cartelp-4, $posY, 4, $cartehp/3, "F");
		}elseif($rclr2!=""){
			$pdf->SetFillColor($rclr2R,$rclr2G,$rclr2B);
			$pdf->Rect($posX+$cartelp-4, $posY+($cartehp/2), 4, $cartehp/2, "F");
			$pdf->SetFillColor($rclr1R,$rclr1G,$rclr1B);
			$pdf->Rect($posX+$cartelp-4, $posY, 4, $cartehp/2, "F");
		}else{
			$pdf->SetFillColor($rclr1R,$rclr1G,$rclr1B);
			$pdf->Rect($posX+$cartelp-4, $posY, 4, $cartehp, "F");
		}
		$pdf->SetTextColor(100,100,100);
		$pdf->SetFont('Arial','',7.5);
		$wd = $pdf->GetStringWidth($domain);
		if($wd>43){$sized = 6.5;}else{$sized = 7.5;}
		$pdf->SetFont('Arial','',$sized);
		$wd = $pdf->GetStringWidth($domain);
		$pdf->TextWithDirection($posX+5,$posY+($cartehp-$wd)/2, $domain, "D");
		$wr = $pdf->GetStringWidth($range);
		if($wr>43){$sizer = 6.5;}else{$sizer = 7.5;}
		$pdf->SetFont('Arial','',$sizer);
		$wr = $pdf->GetStringWidth($range);
		$pdf->TextWithDirection($posX+$cartelp-5,$posY+$wr+($cartehp-$wr)/2, $range, "U");
		// contour de la carte
		$pdf->SetDrawColor(200,200,200);
		$pdf->Rect($posX, $posY, $cartelp, $cartehp, "D");

		// Nom de l'entité
		$pdf->SetXY($posX,$posY+8);
		$pdf->SetFont('Arial','B',9.5);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell($cartelp, 3, $propname, 0, 1, "C", False);
		// Propriété inverse
		if(array_key_exists($codeprop."i", $propi)){
			$inverse = $propi[$codeprop."i"];
			$icode   = substr($inverse, 0, strpos($inverse, "_"));
			$iname   = str_replace("_"," ",substr($inverse, strpos($inverse, "_")+1));
			$wc      = $pdf->GetStringWidth($icode);
			$wt      = $pdf->GetStringWidth($iname);
			$pdf->TextWithDirection($posX+$wt+($cartelp-$wt)/2,$posY+$cartehp-11, $iname, "L");
			$pdf->SetFillColor(255,255,255);
			$pdf->Polygon(array(
				$posX+($cartelp/3)+2.5,$posY+$cartehp-7,
				$posX+(2*($cartelp/3)),$posY+$cartehp-7,
				$posX+(2*($cartelp/3))-2.5,$posY+$cartehp-7+2.5,
				$posX+(2*($cartelp/3)),$posY+$cartehp-7+5,
				$posX+($cartelp/3)+2.5,$posY+$cartehp-7+5,
				$posX+($cartelp/3),$posY+$cartehp-7+2.5),
				'F');
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(150,150,150);
			$pdf->TextWithDirection($posX+1+$wc+($cartelp-$wc)/2,$posY+$cartehp-9+3.25, $icode, "L");
			$pdf->SetFont('Arial','B',8);
			$pdf->SetTextColor(255,255,255);
			$pdf->TextWithDirection($posX+($cartelp/3)-2,$posY+$cartehp-5.5, "RANGE", "L");
			$pdf->TextWithDirection($posX+$cartelp-9.5,$posY+$cartehp-5.5, "DOMAIN", "L");
		}
		
		// Superproperties & subproperties
		// Superclasses
		foreach($pprop as $info => $value){
			if(is_array($value) == true){
				$pdf->SetFont('Arial','',5.5);
				$pdf->SetTextColor(150,150,150);
				$pdf->SetXY($posX+10,$posY+12);
				foreach($value as $parent){
					$pdf->SetFillColor(255,255,255);
					$parentname = str_replace("_"," ",$parent);
					$pdf->Cell($cartelp-20, 2.1, "^  ".$parentname, 0, 1, "L", True);
					$pdf->SetX($posX+10);
				}
				$high = $pdf->GetY();					
				$pdf->SetDrawColor(200,200,200);
				$pdf->SetLineWidth(0.2);
				$pdf->Line($posX+10,$high,$posX+$cartelp-10,$high);
			}else{
				$pdf->SetXY($posX+10,$posY+12);
			}
		}
		// Sous-classes
		$pdf->SetFont('Arial','',5.5);
		$pdf->SetTextColor(150,150,150);
		if(array_key_exists($pid, $isa)){
			$high = $pdf->GetY();					
			$pdf->Line($posX+10,$high,$posX+$cartelp-10,$high);
			foreach($isa[$pid] as $child){
				$pdf->SetX($posX+10);				
				$pdf->SetFillColor(255,255,255);
				$childname = str_replace("_"," ",$child);
				$pdf->Cell($cartelp-20, 2.1, "v  ".$childname, 0, 1, "L", True);
			}
		}
		// gestion des colonnes, lignes et pages
		$colonne++;
		if($colonne==$nbrecol){$colonne=0;$ligne++;}
		if($ligne==$nbrelig){
			$ligne=0;
			$page++;
		}
		// pages paires = commentaires au dos de la carte.
		if(is_int($page/2)== true){
			$pdf->AddPage($orientationp, $format);
			foreach($comm as $dos){
				$pid       = $dos[0];
				$codepropriete = substr($pid, 0, strpos($pid, "_"));
				// version de l'ontologie ou de l'extension
				$codeext = preg_replace('/[^A-Z]/', '', $codepropriete);
				$modelname = version($codeext,$model);
				$modelversion = $model[$modelname]["version"];
				// texte
				$scopenote = $dos[1];
				$posX = $margel+(($nbrecol-1-$colonne)*$cartelp);
				$posY = $margeh+($ligne*$cartehp);
				// contour des cartes
				$pdf->SetDrawColor(250,250,250);
				$pdf->Rect($posX, $posY, $cartelp, $cartehp, "D");
				// nom de l'entité
				$pdf->SetXY($posX,$posY+4);
				$pdf->SetFont('Arial','B',8);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFillColor(200,200,200);
				$propname = str_replace("_"," ",$pid);
				$pdf->Cell($cartelp, 8, " ".$propname, 0, 1, "L", True);
				// ajout du QRcode
				if(file_exists($qrcode."/".$codepropriete.".png")==true){
					$pdf->Image($qrcode."/".$codepropriete.".png", $posX+$cartelp-11 , $posY+3 , 10, 10, "PNG");
				}
				// scope-note
				$pdf->SetXY($posX+3,$posY+15);
				$pdf->SetFont('DejaVu','',7);
				$pdf->MultiCell($cartelp-6, 3, $scopenote, 0, "J");
				// signature
				$pdf->SetXY($posX-8,$posY+$cartehp-3);
				$pdf->SetFont('Arial','',4);
				$pdf->Cell($cartelp-1, 0, "CIDOC ".$modelname." ".$modelversion, 0, 0, "R");
				$pdf->Image($repimg."/".$modelname.".png", $posX+$cartelp-8 , $posY+$cartehp-7 , 5, 0, "PNG");
				// gestion des colonnes, lignes et pages
				$colonne++;
				if($colonne==$nbrecol){$colonne=0;$ligne++;}
				if($ligne==$nbrelig){
					$ligne=0;
					$page++;
					$pdf->AddPage($orientationp, $format);
				}
			}
			unset($comm);
		}
	}
}
// tester s'il ne reste pas des scope-notes à imprimer.
if(isset($comm)){
	$pdf->AddPage($orientationp, $format);
	$colonne=0; $ligne=0; 
	foreach($comm as $dos){
		$pid       = $dos[0];
		$codepropriete = substr($pid, 0, strpos($pid, "_"));
		// version de l'ontologie ou de l'extension
		$codeext = preg_replace('/[^A-Z]/', '', $codepropriete);
		$modelname = version($codeext,$model);
		$modelversion = $model[$modelname]["version"];
		// texte
		$scopenote = $dos[1];
		$posX = $margel+(($nbrecol-1-$colonne)*$cartelp);
		$posY = $margeh+($ligne*$cartehp);
		// contour des cartes
		$pdf->SetDrawColor(250,250,250);
		$pdf->Rect($posX, $posY, $cartelp, $cartehp, "D");
		// nom de l'entité
		$pdf->SetXY($posX,$posY+4);
		$pdf->SetFont('Arial','B',8);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFillColor(200,200,200);
		$propname = str_replace("_"," ",$pid);
		$pdf->Cell($cartelp, 8, " ".$propname, 0, 1, "L", True);
		// ajout du QRcode
		if(file_exists($qrcode."/".$codepropriete.".png")==true){
			$pdf->Image($qrcode."/".$codepropriete.".png", $posX+$cartelp-11 , $posY+3 , 10, 10, "PNG");
		}
		// scope-note
		$pdf->SetXY($posX+3,$posY+15);
		$pdf->SetFont('DejaVu','',7);
		$pdf->MultiCell($cartelp-6, 3, $scopenote, 0, "J");
		// signature
		$pdf->SetXY($posX-8,$posY+$cartehp-3);
		$pdf->SetFont('Arial','',4);
		$pdf->Cell($cartelp-1, 0, "CIDOC ".$modelname." ".$modelversion, 0, 0, "R");
		$pdf->Image($repimg."/".$modelname.".png", $posX+$cartelp-8 , $posY+$cartehp-7 , 5, 0, "PNG");
		// gestion des colonnes, lignes et pages
		$colonne++;
		if($colonne==$nbrecol){$colonne=0;$ligne++;}
	}
	unset($comm);
}

$pdf->Output();

?>