<?php
/*********************************************************
Auteur : Olivier Marlet
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date première version : 19/12/2023
Date dernière mise à jour : 08/11/2024
Objectif : Récupérer la liste des entités et propriétés en CSV (avec une quantité = 1 par défaut pour chaque élément, en prévision de créer automatiquement un fichier PDF avec les cartes des entités à partir du fichier RDF de l'ontologie du CIDOC CRM. Le contenu généré doit être copié dans un fichier TXT renommé en CSV. Il est alors simple de modifier les quantités souhaitées pour chaque carte indépendamment.
Versions logiciels : PHP 8.2.13
*********************************************************/

// #### PARAMETRES ####

// éditer le fichier parameters.php pour indiquer vos préférences
include("CIDOCcard_parameters.php");
// url du fichier de sortie
$csvname = "CIDOCcard_quantity.csv";

// #### TABLEAU DE DONNÉES ####

// Fusion du fichier CRM avec celui des Primitves Values
if($pvfile!=""){
    $doc1 = new DOMDocument();
    $doc1->load($file);

    $doc2 = new DOMDocument();
    $doc2->load($pvfile);

    // get 'rdf:RDF' element of document 1
    $res1 = $doc1->getElementsByTagName('RDF')->item(0);
    // iterate over 'rdfs:Class' elements of document 2
    $items2 = $doc2->getElementsByTagName('Class');
    for ($i = 0; $i < $items2->length; $i ++) {
        $item2 = $items2->item($i);
		// import/copy item from document 2 to document 1
		$item1 = $doc1->importNode($item2, true);
		// append imported item to document 1 'rdf:RDF' element
		$res1->appendChild($item1);
    }
    // iterate over 'rdf:Property' elements of document 2
    $props2 = $doc2->getElementsByTagName('Property');
    for ($i = 0; $i < $props2->length; $i ++) {
        $prop2 = $props2->item($i);
        // import/copy item from document 2 to document 1
        $prop1 = $doc1->importNode($prop2, true);
        // append imported item to document 1 'rdf:RDF' element
        $res1->appendChild($prop1);
    }
	$doc1->save('merged.xml');
	$file = "merged.xml";
}


// tableau contenant les noms de chaque classe et propriété
function lire_classe($file){
    $xml = new XMLReader();
    $xml->open($file, "r");
	global $langue;
	$eltonto = array();
	while($xml->read()) {
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:Class"){
			$eltonto[] = $xml->getAttribute('rdf:about');
		}
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdf:Property"){
			$eltonto[] = $xml->getAttribute('rdf:about');
		}
	}
	return $eltonto;
	$xml->XMLReader::close;
}

// #### CREATION DU FICHIER CSV ####
// ouverture du fichier en écriture
$csv = fopen($csvname, "w");

// séparateur
$sep = ";";

$onto = lire_classe($file);
sort($onto, SORT_NATURAL);
$i = 0;
$prec="";
foreach($onto as $idp => $elt){
	$i++;
	$code=substr($elt,0,strpos($elt,"_"));
	if(strpos($code,"i")===FALSE){
		if($i <= sizeof($onto) && $elt!=$prec ){
			fwrite($csv, $elt . $sep . "1\n");
		}
	}
	$prec = $elt;
}

fclose($csv);

if(is_file($csvname)==true){
	print("Le fichier <a href='$csvname' target='_blank'>$csvname</a> a été créé avec succès dans le même répertoire que ce script.");
}else{
	print("Erreur lors du traitement du script. Vérifiez l'URL du fichier source et si celui-ci ne comporte aucune erreur de syntaxe.");
}

?>