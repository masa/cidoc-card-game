﻿<?php
/*********************************************************
Auteur : Olivier Marlet
Contact : olivier.marlet@univ-tours.fr
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date : 19/12/2023
Objectif : Test de l'impressions recto-verso pour régler les marges et affiner l'adéquation du recto avec le verso
Versions logiciels : PHP 8.2.13, librairie FPDF 1.86
*********************************************************/

// #### PARAMETRES ####

// décalage recto-verso :
$dclhor = -2; // décalage horizontal en mm
$dclver = 0; // décalage vertical en mm

// PAGES : A4 = 210mm x 297mm ; Letter = 215.9mm x 279.4mm
$orientation = "Portrait"; // Landscape ou Portrait
$format = "A4"; // A3 ou Letter
$mapage =array($orientation, $format);
switch($mapage){
	case array("Portrait","A4") :      $pageh = 297;   $pagel = 210;   break;
	case array("Landscape","A4") :     $pageh = 210;   $pagel = 297;   break;
	case array("Portrait","Letter") :  $pageh = 279.4; $pagel = 215.9; break;
	case array("Landscape","Letter") : $pageh = 215.9; $pagel = 279.4; break;
	default : $pagehp = 297;   $pagelp = 210;
}
$pagemh = 20+($dclver/2); // marge haute
$pagemd = 20-($dclhor/2); // marge droite
$pagemb = 20-($dclver/2); // marge basse
$pagemg = 20+($dclhor/2); // marge gauche

require('fpdf/fpdf.php');

$pdf = new FPDF();


///////////////// ICI COMMENCE LE CONTENU POUR LA CREATION DU PDF /////////////////////

// RECTO
$pdf->AddPage($orientation, $format);
// ici écriture de la carte
$posX = $pagemg;
$posY = $pagemh;
$cadreX = $pagel-($pagemd+$pagemg);
$cadreY = $pageh-($pagemh+$pagemb);
// cadre recto
$pdf->SetDrawColor(200,200,200);
$pdf->SetFillColor(255,255,255);
$pdf->Rect($posX, $posY, $cadreX, $cadreY, "D");
// traits de réglage
for($x=$posX-5 ; $x<$posX+6 ; $x++){
	$pdf->Line($x,$posY+5,$x,$posY+7);
}
for($y=$posY-5 ; $y<$posY+6 ; $y++){
	$pdf->Line($posX+5,$y,$posX+7,$y);
}
$pdf->SetFont('Arial','',8);
$pdf->SetXY($posX-5,$posY+5);
$pdf->Cell(5, 10, "-", 0, 1, "C", False);
$pdf->SetXY($posX,$posY+5);
$pdf->Cell(5, 10, "+", 0, 0, "C", False);
$pdf->Cell(50, 10, "horizontal", 0, 0, "L", False);
$pdf->SetXY($posX+9,$posY-7);
$pdf->Cell(2, 10, "-", 0, 1, "C", False);
$pdf->SetXY($posX+9,$posY-2);
$pdf->Cell(2, 10, "+", 0, 0, "C", False);
$pdf->Cell(50, 10, "vertical", 0, 0, "L", False);
// instructions
$pdf->SetXY($posX+20,$posY+20);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell($cadreX-40, 4, iconv('UTF-8', 'ISO-8859-1', "Cette page vous permet de corriger le decalage entre le recto et le verso lors de l'impression :\n\n1. Imprimez cette page en recto-verso sur papier fin.\n2. Regardez par transparence le decalage du verso par rapport au recto.\n3. Notez le decalage en millimetre dans les parametres en en-tete de ce fichier.\n4. Quand l'alignement du recto et du verso est satisfaisant, reportez ces parametres en en-tete de chacun des fichiers \"CIDOCcard\".\n\n\n\n\n\nThis page allows you to correct the offset between front and reverse side when printing:\n\n1. Print this page double-sided (recto-verso) on thin paper.\n2. Look through the transparency to see how the reverse side is offset from the front side.\n3. Note the offset in millimeters in the header parameters of this file.\n4. When the front and back of this page are perfectly aligned, transfer these parameters to the header of each \"CIDOCcard\" file."), 0, "J");

// VERSO
$pdf->AddPage($orientation, $format);
// ici écriture de la carte
$posX = $pagemg;
$posY = $pagemh;
$cadreX = $pagel-($pagemd+$pagemg);
$cadreY = $pageh-($pagemh+$pagemb);
// cadre verso
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(255,255,255);
$pdf->Rect($posX, $posY, $cadreX, $cadreY, "D");

$pdf->Output();

?>