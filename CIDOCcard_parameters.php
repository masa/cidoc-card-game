<?php
/*********************************************************
Auteur : Olivier Marlet
Contact : olivier.marlet@univ-tours.fr
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date première version : 19/12/2023
Date dernière mise à jour : 08/11/2024
Objectif : Créer automatiquement un fichier PDF avec les cartes à jouer représentant les entités et les propriétés à partir du fichier RDF de l'ontologie du CIDOC CRM.
Versions logiciels : PHP 8.2.13, librairie FPDF 1.86
*********************************************************/

// #### PARAMETRES ####

/*******************************
 Notez bien que tout changement de paramètres concernant l'ontologie implique de relancer aussi les scripts :
 1- CIDOCcard_branches_definer.php
 2- CIDOCcard_quantity_exportCSV.php
 en particulier si ajout/retrait du fichier des primitives Values
 *******************************/

////////////////////////
// FICHIERS ONTOLOGIE //
////////////////////////

// url du fichier rdf par rapport à ce fichier
$file = "CidocCRM/CIDOC_CRM_v7.1.2_bundleSci-Archaeo-BA-Dig.rdf";
// url du fichier RDF avec les compléments de Primitives Values ; laisser vide si non souhaité ou déjà intégré au RDF
$pvfile = "";//CidocCRM/CIDOC_CRM_v7.1.2_PrimitivesValues.rdf";
// url du fichier de couleur des entités (pour domaine et co-domaine)
$clr_entities = "CIDOCcard_entity_branchs.xml";
// url du fichier des quantités souhaitées
$quantityfile = "CIDOCcard_quantity.csv";
// url du dossier des images (icônes)
$repimg = "img";
// url du dossier des QRcodes
$qrcode = "qrcode";
// langue souhaitée :
$langue = "en"; // en, fr, de, el, ru, pt, zh
// nombre de caractères limites pour la description au dos
	//// entités
	$txtmaxe = 710;
	//// propriétés
	$txtmaxp = 365;
// les couleurs et nom des icônes sont à changer dans >>> CIDOCcard_colors.php <<<

// versions de l'ontologie et des extensions
	// $ext["nom de l'extension"]     = ("version", "code entité", "code propriété");
$model["CRM"]        = array("version" => "7.1.2", "entite" => "E",    "propriete" => "P");
$model["CRMsci"]     = array("version" => "2.0",   "entite" => "S",    "propriete" => "O");
$model["CRMarchaeo"] = array("version" => "2.0",   "entite" => "A",    "propriete" => "AP");
$model["CRMba"]      = array("version" => "2.0",   "entite" => "B",    "propriete" => "BP");
$model["CRMinf"]     = array("version" => "1.0",   "entite" => "I",    "propriete" => "J");
$model["CRMgeo"]     = array("version" => "1.2",   "entite" => "SP",   "propriete" => "Q");
$model["CRMdig"]     = array("version" => "4.0",   "entite" => "D",    "propriete" => "L");
$model["CRMtex"]     = array("version" => "2.0",   "entite" => "TX",   "propriete" => "TXP");
$model["CRMact"]     = array("version" => "0.2",   "entite" => "actE", "propriete" => "actP");

////////////////////////
// FICHIERS INSTANCES //
////////////////////////

// url du fichier XML d'instances par rapport à ce fichier
$json = "../StreamingAssets/scenarii/Louvre FR/Instances/Instances.json";
// url du dossier des images
$dossier_images = "../StreamingAssets/scenarii/Louvre FR/Instances/Images";
// NOM DE L'INSTANCE :
$scenario= "Louvre";
// url du logo au dos
$logo = "img/OMG_logo.png";
// url du site web
$siteweb = "https://ontomatchgame.huma-num.fr/";

/////////////////////////////////////////////////////////////
// PAGES : A4 = 210mm x 297mm ; Letter = 215,9mm x 279,4mm //
/////////////////////////////////////////////////////////////

$format = "A4"; // A4 ou Letter
// orientation pour les pages ENTITES
$orientatione = "Portrait"; // Portrait ou Landscape
// orientation pour les pages PROPRIETES
$orientationp = "Landscape"; // Portrait ou Landscape
// orientation pour les pages INSTANCES
$orientationi = "Landscape"; // Portrait ou Landscape


// décalage recto-verso : (utiliser le script CIDOCcard_margin-test.php pour contrôler l'alignement recto-verso de votre imprimante)
$dclhor = 0; // décalage horizontal en mm
$dclver = 0; // décalage vertical en mm

// taille des marges en mm en fonction d'un éventuel décalage du recto par rapport au verso
$pagemh = 5+($dclver/2); // marge haute
$pagemd = 5-($dclhor/2); // marge droite
$pagemb = 5-($dclver/2); // marge basse
$pagemg = 5+($dclhor/2); // marge gauche

////////////////////////////////////////////////////////////////////
// CARTES : petites = 43.5mm x 67.5mm ; grandes = 60.0mm x 87.7mm //
////////////////////////////////////////////////////////////////////

// CARTES entités
$cartehe = 87.7; // hauteur carte
$cartele = 60.0; // largeur carte
// CARTES propriétés 
$cartehp = 43.5; // hauteur carte
$cartelp = 67.5; // largeur carte
// CARTES instances
$cartehi = 65.0; // hauteur carte
$carteli = 50.0; // largeur carte


?>