Two important points:
- class "E33_E41_Linguistic_Appellation" cannot be processed by the script automation because of its double name: it is recommended to delete it from the file for proper script execution.
- the RDF version of CIDOC 7.1.2 excludes "Primitive_Value" classes. If you wish to include them for the card game, copy the file here "CIDOC_CRM_v7.1.2_PrimitivesValues.rdf" and make sure to give the url in the parameters at the start of the script (entity cards and property cards). 
