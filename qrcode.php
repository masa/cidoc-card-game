<?php
/*********************************************************
Auteur : Olivier Marlet
Contact : olivier.marlet@univ-tours.fr
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date : 17/01/2024
Objectif : Générer automatiquement les image PNG des QRcode pour chaque entité et propriété du CIDOC CRM.
Versions logiciels : PHP 8.2.13
*********************************************************/

// #### PARAMETRES ####
// url du fichier CSV listant les éléments de l'ontologie (celui des quantités).
$listeitems = "CIDOCcards_quantite.csv";
// nom du dossier contenant les QRcode générés
$repQR = "qrcode/";
// url de référence pour l'ontologie
$ontologie = "https://cidoc-crm.org/html/cidoc_crm_v7.1.2.html";



// librairie pour générer les QRcodes : https://phpqrcode.sourceforge.net/
require("phpqrcode.php");

// tableau des classes et propriétés avec leur code
function lire_liste($listeitems){
	$fic = fopen($listeitems, 'r');
	$liste = array();
	for ($ligne = fgetcsv($fic, 1024); !feof($fic); $ligne = fgetcsv($fic, 1024)) {
		$j = sizeof($ligne);
		$entnm = $ligne[0];
		$code  = substr($entnm, 0, strpos($entnm, "_"));
		$liste[$entnm]=$code;
	}
	return $liste;
}

// récupération du tableau des codes
$liste = lire_liste($listeitems);

// création du répertoir s'il n'existe pas
if(!is_dir("qrcode")){
	mkdir("qrcode",0777, true);
}

// génération de chaque QRcode à partir du code de l'item
foreach($liste as $name => $code){
	$item = $liste[$name];
	$url  = $ontologie."#".$item;
	$save = $repQR.$item.".png";

	QRcode::png($url, $save, 'L', 4, 1);
	// on s'assure du fonctionnement en affichant le PNG généré
	print("<img src='$save' title='$item' alt='$item' width=50 />");
}

?>