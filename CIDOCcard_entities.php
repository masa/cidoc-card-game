<?php
/*********************************************************
Auteur : Olivier Marlet
Contact : olivier.marlet@univ-tours.fr
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date première version : 19/12/2023
Date dernière mise à jour : 08/11/2024
Objectif : Créer automatiquement un fichier PDF avec les cartes des entités à partir du fichier RDF de l'ontologie du CIDOC CRM.
Versions logiciels : PHP 8.2.13, librairie FPDF 1.86
*********************************************************/

// #### PARAMETRES ####

// éditer le fichier parameters.php pour indiquer vos préférences
include("CIDOCcard_parameters.php");

// FORMAT page
$mapage =array($orientatione, $format);
switch($mapage){
	case array("Portrait","A4") :      $pageh = 297;   $pagel = 210;   break;
	case array("Landscape","A4") :     $pageh = 210;   $pagel = 297;   break;
	case array("Portrait","Letter") :  $pageh = 279.4; $pagel = 215.9; break;
	case array("Landscape","Letter") : $pageh = 215.9; $pagel = 279.4; break;
	default : $pageh = 297;   $pagel = 210;
}

// #### FONCTIONS ####

// tableau des couleurs et icônes à partir des branches
function lire_couleur($clr_entities){
	// Couleur et icones associées aux branches
	include("CIDOCcard_colors.php");
	// exploitation du fichier de référence des branches
	$xml = new XMLReader();
    $xml->open($clr_entities, "r");
	while ($xml->read()) {
		if($xml->nodeType == XMLReader::END_ELEMENT) {
			continue;
		}
		if($xml->name == "entity"){
			// S'il s'agit de l'entité qui nous intéresse...
			$entity = $xml->getAttribute('code');
			// ... on récupère les branches associées
			$branches = $xml->getAttribute('branch');
			$brchtab = explode(",",$branches);
			// initialisation des informations
			$clr1="";$clr2="";$clr3="";
			$icn1="";$icn2="";$icn3="";
			// pour chaque branche :
			for($i=0;$i<count($brchtab);$i++){
				$num = $i+1;
				// récupération de la couleur
				${"clr$num"} = $style[$brchtab[$i]][0];
				// récupération de l'icône
				${"icn$num"} = $style[$brchtab[$i]][1];
				$tabclrcla["$entity"] = array(
					"color1"=>"$clr1",
					"color2"=>"$clr2",
					"color3"=>"$clr3",
					"icon1"=>"$icn1",
					"icon2"=>"$icn2",
					"icon3"=>"$icn3"
				);
			}
		}
	}
	return $tabclrcla;
}

// mise en page
$nbrecol = floor(($pagel-$pagemd-$pagemg)/$cartele); // nbre de cartes par colonne
$margel  = ($pagel-($nbrecol*$cartele))/2; // marges latérales (de chaque côté)
$nbrelig = floor(($pageh-$pagemh-$pagemb)/$cartehe); // nbre de cartes par ligne
$margeh  = ($pageh-($nbrelig*$cartehe))/2; // marges longitudinales (en haut et en bas)
$parpage = $nbrecol*$nbrelig; // nombre de cartes par page

require('fpdf/tfpdf.php');

class PDF_Polygon extends tFPDF
{

function Polygon($points, $style='D')
{
    //Draw a polygon
    if($style=='F')
        $op = 'f';
    elseif($style=='FD' || $style=='DF')
        $op = 'b';
    else
        $op = 's';

    $h = $this->h;
    $k = $this->k;

    $points_string = '';
    for($i=0; $i<count($points); $i+=2){
        $points_string .= sprintf('%.2F %.2F', $points[$i]*$k, ($h-$points[$i+1])*$k);
        if($i==0)
            $points_string .= ' m ';
        else
            $points_string .= ' l ';
    }
    $this->_out($points_string . $op);
}

}

$pdf = new PDF_Polygon();

// #### TABLEAUX DE DONNÉES ####

// tableau des relations hiérarchiques pour récupérer les sous-classes d'une classe, à partir du fichier RDF
function IsA($file){
    $xml = new XMLReader();
    $xml->open($file, "r");
	$isa = array();
	while($xml->read()) {
		// récupération des infos d'une entité
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:Class"){
			$entite = array();
			$class = $xml->getAttribute('rdf:about');
		}
		// récupération des super-classes
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:subClassOf"){
			$parent = $xml->getAttribute('rdf:resource');
			$isa[$parent][] = $class;
		}
		// fin des entités >> début de propriétés
		if ($xml->name == "rdf:Property"){
			break;
		}
	}
	return $isa;
	$xml->XMLReader::close;
}

// tableau contenant les informations de chaque classe
function lire_classe($file,$color){
    $xml = new XMLReader();
    $xml->open($file, "r");
	global $langue;
	$entites = array(); // tableau général
	while($xml->read()) {
		// récupération des infos d'une entité
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:Class"){
			$entite = array(); // tableau des infos d'une entité
			$class = $xml->getAttribute('rdf:about');
			$numcl = substr($class,0, strpos($class, "_"));
		}
		// récupération du nom de l'entité en fonction de la langue demandée
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:label" && $xml->getAttribute('xml:lang')=="$langue"){
			$entite["classname"] = $xml->readString();
			
			// récupération des icones et couleurs
			$entite["clr1"] = $color["$numcl"]["color1"];
			$entite["clr2"] = $color["$numcl"]["color2"];
			$entite["clr3"] = $color["$numcl"]["color3"];
			$entite["icon1"] = $color["$numcl"]["icon1"];
			$entite["icon2"] = $color["$numcl"]["icon2"];
			$entite["icon3"] = $color["$numcl"]["icon3"];
		}
		// récupération de la description
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:comment"){
			$entite["comment"] = $xml->readString();
		}
		// récupération des super-classes
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:subClassOf"){
			$entite["super"][] = $xml->getAttribute('rdf:resource');
		}
		// ajout des infos de l'entité au tableau général
		if ($xml->nodeType == XMLReader::END_ELEMENT && $xml->name == "rdfs:Class"){
			$entites[$class] = $entite;
		}
	}
	return $entites;
	$xml->XMLReader::close;
}

// tableau des quantités souhaitées de chaque classe
function lire_quantite($quantityfile){
	$fic = fopen($quantityfile, 'r');
	$quantity = array();
	for ($ligne = fgetcsv($fic, 1024, ";"); !feof($fic); $ligne = fgetcsv($fic, 1024, ";")) {
		$j = sizeof($ligne);
		$entnm = $ligne[0];
		$qtt   = $ligne[1];
		$quantity[$entnm]=$qtt;
	}
	return $quantity;
}

// Fusion du fichier CRM avec celui des Primitives Values
if($pvfile!=""){
    $doc1 = new DOMDocument();
    $doc1->load($file);

    $doc2 = new DOMDocument();
    $doc2->load($pvfile);

    // get 'rdf:RDF' element of document 1
    $res1 = $doc1->getElementsByTagName('RDF')->item(0);
    // iterate over 'rdfs:Class' elements of document 2
    $items2 = $doc2->getElementsByTagName('Class');
    for ($i = 0; $i < $items2->length; $i ++) {
        $item2 = $items2->item($i);
        // import/copy item from document 2 to document 1
        $item1 = $doc1->importNode($item2, true);
        // append imported item to document 1 'rdf:RDF' element
        $res1->appendChild($item1);
    }
    // iterate over 'rdf:Property' elements of document 2
    $props2 = $doc2->getElementsByTagName('Property');
    for ($i = 0; $i < $props2->length; $i ++) {
        $prop2 = $props2->item($i);
        // import/copy item from document 2 to document 1
        $prop1 = $doc1->importNode($prop2, true);
        // append imported item to document 1 'rdf:RDF' element
        $res1->appendChild($prop1);
    }
	$doc1->save('merged.xml');
	$file = "merged.xml";
}

// récupération de la version de l'ontologie
function version($codeentite, $array){
	foreach($array as $key => $val){
		if($val["entite"] === $codeentite){
			return $key;
		}
	}
}

///////////////// ICI COMMENCE LE CONTENU POUR LA CREATION DU PDF /////////////////////

// récupération du tableau des relations IsA
$isa = isa($file);
// récupération des informations sur les couleurs et les icônes associées
$color= lire_couleur($clr_entities);
// stockage de l'ontologie dans un tableau associatif
$onto = lire_classe($file,$color);
// récupération du tableau des quantités
$quantity = lire_quantite($quantityfile);
// initialisation
$colonne=0;$ligne=0;$page=1;

$pdf->AddPage($orientatione, $format);
// Add a Unicode font (uses UTF-8)
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
foreach($onto as $pid => $pentite){
	// Gestion de la quantité de cartes à créer pour chaque entité
	$qtt = $quantity[$pid];
	for($q=1; $q<=$qtt; $q++){
		// récupération de l'information depuis le tableau
		// boucle pour chaque entité
		$codeentite = substr($pid, 0, strpos($pid, "_"));
		$classname  = $onto[$pid]["classname"];
		$clr1       = $onto[$pid]["clr1"];
		$clr2       = $onto[$pid]["clr2"];
		$clr3       = $onto[$pid]["clr3"];
		$icon1      = $onto[$pid]["icon1"];
		$icon2      = $onto[$pid]["icon2"];
		$icon3      = $onto[$pid]["icon3"];
		$clr1R      = base_convert(substr($clr1,1,2),16,10);
		$clr1G      = base_convert(substr($clr1,3,2),16,10);
		$clr1B      = base_convert(substr($clr1,5,2),16,10);
		$moyenne    = floor(($clr1R+$clr1G+$clr1B)/3);
		$clr2R      = base_convert(substr($clr2,1,2),16,10);
		$clr2G      = base_convert(substr($clr2,3,2),16,10);
		$clr2B      = base_convert(substr($clr2,5,2),16,10);
		$clr3R      = base_convert(substr($clr3,1,2),16,10);
		$clr3G      = base_convert(substr($clr3,3,2),16,10);
		$clr3B      = base_convert(substr($clr3,5,2),16,10);
		$scopenote  = $onto[$pid]["comment"];
		if(strlen($scopenote)>$txtmaxe){
			$scopenote = substr($scopenote,0,$txtmaxe);
			$scopenote = substr($scopenote,0,strrpos($scopenote," "))."...";
		}
		$comm[] = array($pid, $scopenote);
		// ici écriture de la carte
		$posX = $margel+($colonne*$cartele);
		$posY = $margeh+($ligne*$cartehe);
		// fond de la carte
		$pdf->SetFillColor(245,245,245);
		$pdf->Rect($posX, $posY, $cartele, $cartehe, "F");
		// titre
		if($icon1!=""){
			$pdf->Image($repimg."/".$icon1.".png", $posX+4 ,$posY+4 , 9, 7.5, "PNG");
		}
		if($icon3!=""){
			$pdf->Image($repimg."/".$icon2.".png", $posX+42 ,$posY+4 , 9, 7.5, "PNG");
			$pdf->Image($repimg."/".$icon3.".png", $posX+50 ,$posY+4 , 9, 7.5, "PNG");			
		}
		elseif($icon2!=""){
			$pdf->Image($repimg."/".$icon2.".png", $posX+47 ,$posY+4 , 9, 7.5, "PNG");
		}
		// numéro d'entité
		$pdf->SetFont('Arial','B',14);
		$pdf->SetXY($posX+($cartele/3),$posY+4);
//		if($moyenne > 200){
			$pdf->SetTextColor(0,0,0);
//		}else{
//			$pdf->SetTextColor(255,255,255);
//		}
		// code couleur en fonction des branches
		$pdf->SetFillColor($clr1R,$clr1G,$clr1B);
		if($clr2==""){
			// 1 seule couleur
			$pdf->Rect($posX+($cartele/3), $posY+4, ($cartele/3), 8, "F");
			// bord gauche
			$pdf->Rect($posX, $posY+22.1, 2.5, 65.6, "F");
			// bord droit
			$pdf->Rect($posX+$cartele-2.5, $posY+22.1, 2.5, 65.6, "F");
		}elseif($clr3==""){
			// 2 couleurs
			$pdf->Polygon(array($posX+($cartele/3),$posY+4,$posX+(2*($cartele/3)),$posY+4,$posX+($cartele/3),$posY+12),'F');
			$pdf->SetFillColor($clr2R,$clr2G,$clr2B);
			$pdf->Polygon(array($posX+(2*($cartele/3)),$posY+4,$posX+($cartele/3),$posY+12,$posX+(2*($cartele/3)),$posY+12,),'F');
			// bord d+g couleur 1
			$pdf->SetFillColor($clr1R,$clr1G,$clr1B);
			$pdf->Rect($posX, $posY+22, 2.5, 32.8, "F");
			$pdf->Rect($posX+$cartele-2.5, $posY+22, 2.5, 32.8, "F");
			// bord d+g couleur 2
			$pdf->SetFillColor($clr2R,$clr2G,$clr2B);
			$pdf->Rect($posX, $posY+54.8, 2.5, 32.8, "F");
			$pdf->Rect($posX+$cartele-2.5, $posY+54.8, 2.5, 32.8, "F");
		}else{
			// 3 couleurs
			$pdf->Polygon(array($posX+($cartele/3),$posY+4,$posX+(1.5*($cartele/3)),$posY+4,$posX+(1.5*($cartele/3)),$posY+8,$posX+($cartele/3),$posY+12),'F');
			$pdf->SetFillColor($clr2R,$clr2G,$clr2B);
			$pdf->Polygon(array($posX+($cartele/3),$posY+12,$posX+(1.5*($cartele/3)),$posY+8,$posX+(2*($cartele/3)),$posY+12,),'F');
			$pdf->SetFillColor($clr3R,$clr3G,$clr3B);
			$pdf->Polygon(array($posX+(1.5*($cartele/3)),$posY+4,$posX+(2*($cartele/3)),$posY+4,$posX+(2*($cartele/3)),$posY+12,$posX+(1.5*($cartele/3)),$posY+8),'F');
			// bord d+g couleur 1
			$pdf->SetFillColor($clr1R,$clr1G,$clr1B);
			$pdf->Rect($posX, $posY+22.1, 2.5, 21.8, "F");
			$pdf->Rect($posX+$cartele-2.5, $posY+22.1, 2.5, 21.8, "F");
			// bord d+g couleur 2
			$pdf->SetFillColor($clr2R,$clr2G,$clr2B);
			$pdf->Rect($posX, $posY+43.9, 2.5, 21.8, "F");
			$pdf->Rect($posX+$cartele-2.5, $posY+43.9, 2.5, 21.8, "F");
			// bord d+g couleur 3
			$pdf->SetFillColor($clr3R,$clr3G,$clr3B);
			$pdf->Rect($posX, $posY+65.7, 2.5, 21.9, "F");
			$pdf->Rect($posX+$cartele-2.5, $posY+65.7, 2.5, 21.9, "F");
		}
		// contour de la carte
		$pdf->SetDrawColor(200,200,200);
		$pdf->Rect($posX, $posY, $cartele, $cartehe, "D");
		$pdf->Cell($cartele/3, 8, $codeentite, 0, 1, "C", False);
		// Nom de l'entité
		$pdf->SetXY($posX,$posY+12);
		$pdf->SetFont('Arial','B',12);		
		$namewidth = $pdf->GetStringWidth($classname);
		if($namewidth > 70) {
			$namesize = 9;
		}elseif($namewidth > 60) {
			$namesize = 10;
		}elseif($namewidth > 50) {
			$namesize = 11;
		}else{
			$namesize = 12;
		}
		$pdf->SetFont('Arial','B',$namesize);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell($cartele, 8, $classname, 0, 1, "C", False);
		// Superclasses & subclasses
		$pdf->SetXY($posX,$posY+20);
		$pdf->SetFont('Arial','B',10);
		$pdf->SetTextColor(180,180,180);
		$pdf->Cell($cartele, 8, "Superclasses & subclasses", 0, 1, "C", False);
		$pdf->SetXY($posX+4,$posY+28);
		// Superclasses
		$pdf->SetFont('Arial','',8);
		$pdf->SetTextColor(100,100,100);
		foreach($pentite as $info => $value){
			if(is_array($value) == true){
				foreach($value as $parent){
					$pdf->SetFillColor(255,255,255);
					$parentname = str_replace("_"," ",$parent);
					$pdf->Cell($cartele-8, 4.3, "^  ".$parentname, 0, 1, "L", True);
					$pdf->SetX($posX+4);				
				}
			}
		}
		// entité de référence
		$pdf->SetFont('Arial','B',8.5);
		$pidname = str_replace("_"," ",$pid);
		$refwidth = $pdf->GetStringWidth($pidname);
		if($refwidth > 55) {
			$refsize = 7;
		}elseif($refwidth > 50) {
			$refsize = 7.5;
		}else{
			$refsize = 8.5;
		}
		$pdf->SetFont('Arial','B',$refsize);
		$pdf->SetTextColor(50,50,50);
		$pdf->SetFillColor(180,180,180);
		$pdf->Cell($cartele-8, 5, $pidname, 0, 1, "L", True);
		$pdf->SetX($posX+4);				
		// Sous-classes
		$pdf->SetTextColor(100,100,100);
		$pdf->SetFillColor(255,255,255);
		if(array_key_exists($pid, $isa)){
			foreach($isa[$pid] as $child){
				$pdf->SetFont('Arial','',8);
				$childname = str_replace("_"," ",$child);
				$childwidth = $pdf->GetStringWidth($childname);
				if($childwidth > 55) {
					$childsize = 7;
				}elseif($childwidth > 50) {
					$childsize = 7.5;
				}else{
					$childsize = 8;
				}
				$pdf->SetFont('Arial','',$childsize);
				$pdf->Cell($cartele-8, 4.3, "v  ".$childname, 0, 1, "L", True);
				$pdf->SetX($posX+4);				
			}
		}
		// gestion des colonnes, lignes et pages
		$colonne++;
		if($colonne==$nbrecol){$colonne=0;$ligne++;}
		if($ligne==$nbrelig){
			$ligne=0;
			$page++;
		}
		// pages paires = commentaires au dos de la carte.
		if(is_int($page/2)== true){
			$pdf->AddPage($orientatione, $format);
			foreach($comm as $dos){
				$pid       = $dos[0];
				$codeentite = substr($pid, 0, strpos($pid, "_"));
				// version de l'ontologie ou de l'extension
				$codeext = preg_replace('/[^A-Z]/', '', $codeentite);
				$modelname = version($codeext,$model);
				$modelversion = $model[$modelname]["version"];
				// texte
				$scopenote = $dos[1];
				$posX = $margel+(($nbrecol-1-$colonne)*$cartele);
				$posY = $margeh+($ligne*$cartehe);
				// contour des cartes
				$pdf->SetDrawColor(250,250,250);
				$pdf->Rect($posX, $posY, $cartele, $cartehe, "D");
				// nom de l'entité
				$pdf->SetXY($posX,$posY+4);
				$pdf->SetFont('Arial','B',9);	
				$titleback = str_replace("_"," ",$pid);
				$titlewidth = $pdf->GetStringWidth($titleback);
				if($titlewidth > 60) {
					$titlesize = 7.5;
				}elseif($titlewidth > 55) {
					$titlesize = 8;
				}elseif($titlewidth > 50) {
					$titlesize = 8.5;
				}else{
					$titlesize = 9;
				}
				$pdf->SetFont('Arial','B',$titlesize);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFillColor(200,200,200);
				$pdf->Cell($cartele, 8, $titleback, 0, 1, "C", True);
				// ajout du QRcode
				if(file_exists($qrcode."/".$codeentite.".png")==true){
					$pdf->Image($qrcode."/".$codeentite.".png", $posX+2 ,$posY+$cartehe-12 , 10, 10, "PNG");
				}
				// scope-note
				$pdf->SetXY($posX+3,$posY+15);
				$pdf->SetFont('DejaVu','',7);
				$pdf->MultiCell($cartele-6, 3, $scopenote, 0, "J");
				// signature
				$pdf->SetXY($posX-8,$posY+$cartehe-3.1);
				$pdf->SetFont('Arial','',4);
				$pdf->Cell($cartele-1, 0, "CIDOC ".$modelname." ".$modelversion, 0, 0, "R");
				$pdf->Image($repimg."/".$modelname.".png", $posX+$cartele-8 ,$posY+$cartehe-7 , 5, 0, "PNG");
				// gestion des colonnes, lignes et pages
				$colonne++;
				if($colonne==$nbrecol){$colonne=0;$ligne++;}
				if($ligne==$nbrelig){
					$ligne=0;
					$page++;
					$pdf->AddPage($orientatione, $format);
				}
			}
			unset($comm);
		}
	}
}
// tester s'il ne reste pas des scope-notes à imprimer.
if(isset($comm)){
	$pdf->AddPage($orientatione, $format);
	$colonne=0; $ligne=0; 
	foreach($comm as $dos){
		$pid       = $dos[0];
		$codeentite = substr($pid, 0, strpos($pid, "_"));
		// version de l'ontologie ou de l'extension
		$codeext = preg_replace('/[^A-Z]/', '', $codeentite);
		$modelname = version($codeext,$model);
		$modelversion = $model[$modelname]["version"];
		// texte
		$scopenote = $dos[1];
		$posX = $margel+(($nbrecol-1-$colonne)*$cartele);
		$posY = $margeh+($ligne*$cartehe);
		// contour des cartes
		$pdf->SetDrawColor(250,250,250);
		$pdf->Rect($posX, $posY, $cartele, $cartehe, "D");
		// nom de l'entité
		$pdf->SetXY($posX,$posY+4);
		$pdf->SetFont('Arial','B',9);
		$titleback = str_replace("_"," ",$pid);
		$titlewidth = $pdf->GetStringWidth($titleback);
		if($titlewidth > 60) {
			$titlesize = 7.5;
		}elseif($titlewidth > 55) {
			$titlesize = 8;
		}elseif($titlewidth > 50) {
			$titlesize = 8.5;
		}else{
			$titlesize = 9;
		}
		$pdf->SetFont('Arial','B',$titlesize);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFillColor(200,200,200);
		$pdf->Cell($cartele, 8, $titleback, 0, 1, "C", True);
		// ajout du QRcode
		if(file_exists($qrcode."/".$codeentite.".png")==true){
			$pdf->Image($qrcode."/".$codeentite.".png", $posX+2 ,$posY+$cartehe-12 , 10, 10, "PNG");
		}
		// scope-note
		$pdf->SetXY($posX+3,$posY+15);
		$pdf->SetFont('DejaVu','',7);
		$pdf->MultiCell($cartele-6, 3, $scopenote, 0, "J");
		// signature
		$pdf->SetXY($posX-8,$posY+$cartehe-3.1);
		$pdf->SetFont('Arial','',4);
		$pdf->Cell($cartele-1, 0, "CIDOC ".$modelname." ".$modelversion, 0, 0, "R");
		$pdf->Image($repimg."/".$modelname.".png", $posX+$cartele-8 ,$posY+$cartehe-7 , 5, 0, "PNG");
		// gestion des colonnes, lignes et pages
		$colonne++;
		if($colonne==$nbrecol){$colonne=0;$ligne++;}
	}
	unset($comm);
}

$pdf->Output();

?>