<?php

// Couleurs et icones associées aux branches
// les codes en commentaire sont : Olivier Marlet;George Bruseker;Philippe Michon

$style = array(
"entity"           => array("#FFFFFF",""),
"conceptual"       => array("#fddc34","iconIdea"),    // E28 fddc34;fddc34;fddc34 -> OK
"actor"            => array("#ffbdca","iconUser"),    // E39 ffbdca;ffbdca;ffbdca -> OK
"place"            => array("#94cc7d","iconLocation"),// E53 94cc7d;94cc7d;94cc7d -> OK
"time-span"        => array("#86bcc8","iconClock"),   // E52 5578af;5578af;86bcc8
"temporal"         => array("#82ddff","iconAxis"),    // E2  82ddff;82c3ec;82c3ec
"physical"         => array("#c78e66","iconCube"),    // E18 9b4d2f;e1ba9c;e1ba9c
"appellation"      => array("#fef3ba","iconID"),      // E41 fddc34;fddc34;fef3ba
"type"             => array("#fab565","iconForms"),   // E55 fddc34;fddc34;fab565
"spacetime-volume" => array("#cc80ff","iconMoebius"), // E92 bc8df0;e5ccff;cc80ff
"dimension"        => array("#cccccc","iconRuler"),   // E54 888888;e6e4ec;cccccc
"primitive"        => array("#e5e0c3","iconPrimitive")// E59 cfccda;x;x;dddddd?
);

?>