<?php
/*********************************************************
Auteur : Olivier Marlet
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date première version : 19/12/2023
Date dernière mise à jour : 08/11/2024
Objectif : à partir du fichier RDF, créer les fichiers de couleurs pour les entités et les propriétés en définissant les têtes de branches.
Versions logiciels : PHP 8.2.13
*********************************************************/

// #### PARAMETRES ####

// éditer le fichier parameters.php pour indiquer vos préférences
include("CIDOCcard_parameters.php");


// #### TABLEAUX DE DONNÉES ####

// Têtes de branches (entités de haut niveau)
$head = array(
"E1" => "entity",
"E2" => "temporal",
"E4" => "temporal",
"E18" => "physical",
"E24" => "physical",
"E28" => "conceptual",
"E39" => "actor",
"E52" => "time-span",
"E53" => "place",
"E54" => "dimension",
"E59" => "primitive",
"E41" => "appellation",
"E55" => "type",
"E92" => "spacetime-volume",
"S10" => "physical"
);

// tableau hiérarchique des entités à partir du fichier XML
function hierarchie($file){
    $xml = new XMLReader();
    $xml->open($file, "r");
	$entites = array(); // tableau général
	while($xml->read()) {
		// récupération des infos d'une entité
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:Class"){
			$class = $xml->getAttribute('rdf:about');
			$code=substr($class,0,strpos($class,"_"));
		}
		// récupération des super-classes
		if ($xml->nodeType == XMLReader::ELEMENT && $xml->name == "rdfs:subClassOf"){
			$parent = $xml->getAttribute('rdf:resource');
			$codep=substr($parent,0,strpos($parent,"_"));
			$entites[$code][] = $codep;
		}
	}
	return $entites;
	$xml->XMLReader::close;
}
// remplacer la lettre représentant le CRM ou l'extension par une valeur
function crm2int($entity){
	$codecrm = substr($entity,0,1);
		switch($codecrm){
			case "A": $numcrm="4"; break;
			case "B": $numcrm="5"; break;
			case "E": $numcrm="1"; break;
			case "S": $numcrm="2"; break;
			case "D": $numcrm="6"; break;
			case "SP": $numcrm="7"; break;
			case "I": $numcrm="8"; break;
		}
	$numcl = $numcrm.str_pad(substr($entity,1),3,"0",STR_PAD_LEFT);
	return $numcl;
}

function int2crm($numcl){
	$numcrm = substr($numcl,0,1);
		switch($numcrm){
			case "1": $codecrm="E"; break;
			case "2": $codecrm="S"; break;
			case "4": $codecrm="A"; break;
			case "5": $codecrm="B"; break;
			case "6": $codecrm="D"; break;
			case "7": $codecrm="SP"; break;
			case "8": $codecrm="I"; break;
		}
	$entity = $codecrm.ltrim(substr($numcl,1), "0");
	return $entity;
}

// exploration de la hierarchie pour affecter aux entités la branche dont elle hérite
function arbre($entity, $isatab, $branch){
	global $head;
	global $arbre;
	$match=0;
	foreach($isatab as $child => $value){
		foreach($value as $super){
			if($super == $entity){
				$match++;
				// on remplace le code CRM par une valeur numérique pour permettre le tri
				$numcl = crm2int($entity);
				
				// si entité de haut niveau, on lui affecte sa branche
				if(array_key_exists($entity,$head)==true){
					$branch = $head[$entity];
				}
				// si la clé n'a pas encore été rencontrée, on la crée
				if(array_key_exists($numcl,$arbre)===false){
					// on crée l'entrée dans le tableau
					$arbre[$numcl]=$branch;
				}else{ // si la clé existe déjà dans la table de vérification
					// mais que la branche déjà associée est différente de la branche actuellement héritée 
					if(strpos($arbre[$numcl],$branch)===false && $arbre[$numcl]!="entity" && $branch!="entity"){
						// on complète l'entrée du tableau avec cette nouvelle branche
						$arbre[$numcl]=$arbre[$numcl].",".$branch;
					}
				}
				// et on relance sur les enfants de cette entité
				arbre($child, $isatab, $branch);
			}
		}
	}
	// fin de branche (= pas de descendance)
	if($match==0){
		$numcl = crm2int($entity);//substr($entity,1);
		// si entité de haut niveau, on lui affecte sa branche
		if(array_key_exists($entity,$head)==true){
			$branch = $head[$entity];
		}
		// si la clé n'a pas encore été rencontrée
		if(array_key_exists($numcl,$arbre)===false){
			// ...on crée la clé dans le tableau
			$arbre[$numcl]=$branch;
		}else{ // si la clé existe déjà dans la table de vérification
			// mais que la branche déjà associée est différente de la branche actuellement héritée 
			if(strpos($arbre[$numcl],$branch)===false && $arbre[$numcl]!="entity" && $branch!="entity"){
				// on complète l'entrée du tableau avec cette nouvelle branche
				$arbre[$numcl]=$arbre[$numcl].",".$branch;
			}
		}
	}
	return $arbre;
}

// Fusion du fichier CRM avec celui des Primitves Values
if($pvfile!=""){
    $doc1 = new DOMDocument();
    $doc1->load($file);

    $doc2 = new DOMDocument();
    $doc2->load($pvfile);

    // get 'rdf:RDF' element of document 1
    $res1 = $doc1->getElementsByTagName('RDF')->item(0);

    // iterate over 'rdfs:Class' elements of document 2
    $items2 = $doc2->getElementsByTagName('Class');
    for ($i = 0; $i < $items2->length; $i ++) {
        $item2 = $items2->item($i);

        // import/copy item from document 2 to document 1
        $item1 = $doc1->importNode($item2, true);

        // append imported item to document 1 'rdf:RDF' element
        $res1->appendChild($item1);
		$br = $doc1->createTextNode("\n\t");
		$res1->appendChild($br);
    }
	$doc1->save('merged.xml');
	$myfile = "merged.xml";
}
else{
	$myfile = $file;
}

// récupération du tableau des hiérarchies ascendantes

$isatab = hierarchie($myfile);

// on reconstruit l'arborescence à partir de E1_CRM_Entity
$arbre = array();
$arbretab = arbre("E1",$isatab, "");
//ksort($arbretab);
ksort($arbretab);


// #### CREATION DU FICHIER XML POUR LES ENTITES ####

// ouverture du fichier en écriture
$xmlstyle = fopen($clr_entities, "w");

fwrite($xmlstyle,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<root>
	<comment>
		Types of branch allowed : 
		- entity (the most generic E1)
		- temporal (temporal entity = E2, E4)
		- physical (physical entity = E18, E24)
		- conceptual (conceptual entity = E28)
		- appellation (E41)
		- type (E55)
		- actor (E39)
		- time-span (E52)
		- place (E53)
		- dimension (E54)
		- primitive (E59)
		- spacetime-volume (E92)
	</comment>
	<entities>");

foreach($arbretab as $eltn => $branch){
	// on rebascule du code numérique au vrai code de l'entité
	$elt = int2crm($eltn);
	$clr1="";$clr2="";$clr3="";
	$icn1="";$icn2="";$icn3="";
	fwrite($xmlstyle,"
		<entity code=\"$elt\" branch=\"$branch\" />");
}

fwrite($xmlstyle,"
	</entities>
</root>");

fclose($xmlstyle);

if(is_file($clr_entities)==true){
	print("Le fichier <a href='$clr_entities' target='_blank'>$clr_entities</a> a été créé avec succès dans le même répertoire que ce script.<br/><br/>");
}else{
	print("Erreur lors du traitement du script pour les entités. Vérifiez l'URL du fichier RDF et si celui-ci ne comporte aucune erreur de syntaxe.<br/><br/>");
}

?>