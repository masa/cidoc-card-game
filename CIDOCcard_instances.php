<?php
/*********************************************************
Auteur : Olivier Marlet
Contact : olivier.marlet@univ-tours.fr
Organisme : UMR7325 CITERES-LAT, CNRS - Université de Tours
Date première version : 19/12/2023
Date dernière mise à jour : 08/11/2024
Objectif : Créer automatiquement un fichier PDF avec les cartes des instances pour réfléchir au mapping avec l'ontologie du CIDOC CRM.
Versions logiciels : PHP 8.2.13, librairie FPDF 1.86
*********************************************************/

// #### PARAMETRES ####

// éditer le fichier parameters.php pour indiquer vos préférences
include("CIDOCcard_parameters.php");

// FORMAT page
$mapage =array($orientationi, $format);
switch($mapage){
	case array("Portrait","A4") :      $pageh = 297;   $pagel = 210;   break;
	case array("Landscape","A4") :     $pageh = 210;   $pagel = 297;   break;
	case array("Portrait","Letter") :  $pageh = 279.4; $pagel = 215.9; break;
	case array("Landscape","Letter") : $pageh = 215.9; $pagel = 279.4; break;
	default : $pagehp = 297;   $pagelp = 210;
}

$nbrecol = floor(($pagel-$pagemd-$pagemg)/$carteli); // nbre de cartes par colonne
$margel  = ($pagel-($nbrecol*$carteli))/2; // marges latérales (de chaque côté)
$nbrelig = floor(($pageh-$pagemh-$pagemb)/$cartehi); // nbre de cartes par ligne
$margeh  = ($pageh-($nbrelig*$cartehi))/2; // marges longitudinales (en haut et en bas)
$parpage = $nbrecol*$nbrelig; // nombre de cartes par page

// si l'instance est passée en variable dans l'URL
if(isset($_GET["name"])&&$_GET["name"]!=""){
	$instance = $_GET["name"];
	$json = "../StreamingAssets/scenarii/$instance/Instances/Instances.json";
	$dossier_images = "../StreamingAssets/scenarii/$instance/Instances/Images";
	$scenario= substr($instance,0,strpos($instance," "));
}

require('fpdf/tfpdf.php');

class PDF_Ellipse extends tFPDF {
	function Circle($x, $y, $r, $style='D')
	{
		$this->Ellipse($x,$y,$r,$r,$style);
	}

	function Ellipse($x, $y, $rx, $ry, $style='D'){
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$lx=4/3*(M_SQRT2-1)*$rx;
		$ly=4/3*(M_SQRT2-1)*$ry;
		$k=$this->k;
		$h=$this->h;
		$this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
			($x+$rx)*$k,($h-$y)*$k,
			($x+$rx)*$k,($h-($y-$ly))*$k,
			($x+$lx)*$k,($h-($y-$ry))*$k,
			$x*$k,($h-($y-$ry))*$k));
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
			($x-$lx)*$k,($h-($y-$ry))*$k,
			($x-$rx)*$k,($h-($y-$ly))*$k,
			($x-$rx)*$k,($h-$y)*$k));
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
			($x-$rx)*$k,($h-($y+$ly))*$k,
			($x-$lx)*$k,($h-($y+$ry))*$k,
			$x*$k,($h-($y+$ry))*$k));
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
			($x+$lx)*$k,($h-($y+$ry))*$k,
			($x+$rx)*$k,($h-($y+$ly))*$k,
			($x+$rx)*$k,($h-$y)*$k,
			$op));
	}
}

$pdf = new PDF_Ellipse();

// #### TABLEAUX DE DONNÉES ####

// tableau des données Json
function lire_instances($json){
	$content = file_get_contents($json);
    $instances = json_decode($content, true);
	return $instances;
}


///////////////// ICI COMMENCE LE CONTENU POUR LA CREATION DU PDF /////////////////////

// récupération du tableau des instances
$instances = lire_instances($json);
	
$colonne=0;$ligne=0;$page=1;
$pdf->AddPage($orientationi, $format);
// Add a Unicode font (uses UTF-8)
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu','B','DejaVuSansCondensed-Bold.ttf',true);

foreach($instances as $instance){
	// récupération de l'information depuis le tableau JSON
	$num       = $instance["Id"];
//	$titre     = iconv('UTF-8', 'ISO-8859-1', $instance["Title"]);
//	$soustitre = iconv('UTF-8', 'ISO-8859-1', $instance["Label"]);
	$titre     = $instance["Title"];
	$soustitre = $instance["Label"];
	$image     = $instance["ImageName"];
	// ici écriture de la carte
	$posX = $margel+($colonne*$carteli);
	$posY = $margeh+($ligne*$cartehi);
	// contour des cartes
	$pdf->SetDrawColor(200,200,200);
	$pdf->SetFillColor(255,255,255);
	$pdf->Rect($posX, $posY, $carteli, $cartehi, "D");
	// numéro d'instance
	$pdf->SetXY($posX+($carteli/3),$posY+2.5);
	$pdf->SetDrawColor(0,0,0);
	$pdf->SetFillColor(200,200,200);
	$pdf->Circle($posX+($carteli/2),$posY+6,4,"F");
	$pdf->SetTextColor(255,255,255);
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell($carteli/3, 8, strtolower($num), 0, 1, "C", False);
	// titre et sous-titre
	$pdf->SetXY($posX,$posY+11);
	$pdf->SetTextColor(0,0,0);
	$tailledefaut = 9;
	$pdf->SetFont('DejaVu','B',$tailledefaut);
	$largeurtitre = $pdf->GetStringWidth($titre);
	if($largeurtitre>90){$tailletitre = round($tailledefaut*90/$largeurtitre, 1);}
	else{$tailletitre = $tailledefaut;}
	$pdf->SetFont('DejaVu','B',$tailletitre);
	$pdf->MultiCell($carteli, 4, $titre, 0, "C", False);
	$pdf->SetX($posX);
	$stdefaut = 8;
	$pdf->SetFont('DejaVu','',$stdefaut);
	$largeurst = $pdf->GetStringWidth($soustitre);
	if($largeurst>47){$taillest = round($stdefaut*47/$largeurst, 1);}
	else{$taillest = $stdefaut;}
	$pdf->SetFont('DejaVu','',$taillest);
	$pdf->MultiCell($carteli, 4, $soustitre, 0, "C", False);
	// illustration
	$pdf->Image($dossier_images."/".$image, $posX+7.5 ,$posY+$cartehi-40 , 35, 35, "JPG");
	
	// gestion des colonnes, lignes et pages
	$dos[]=	array($num);
	$colonne++;
	if($colonne==$nbrecol){$colonne=0;$ligne++;}
	if($ligne==$nbrelig){
		$ligne=0;
		$page++;
	}
	// pages paires = nom de l'instance et logo au dos de la carte.
	if(is_int($page/2)== true){
		$pdf->AddPage($orientationi, $format);
		for($i=1;$i<=$parpage;$i++){
			$posX = $margel+(($nbrecol-1-$colonne)*$carteli);
			$posY = $margeh+($ligne*$cartehi);
			$pdf->SetXY($posX,$posY+8);
			$pdf->SetTextColor(200,200,200);
			$scdefaut = 30;
			$pdf->SetFont('Arial','B',$scdefaut);
			$largeursc = $pdf->GetStringWidth($scenario);
			if($largeursc>40){$taillesc = round($scdefaut*40/$largeursc, 1);}
			else{$taillesc = $scdefaut;}
			$pdf->SetFont('Arial','B',$taillesc);
			$pdf->Cell($carteli, 8, $scenario, 0, 0, "C", false);
			$pdf->Image($logo, $posX+10 ,$posY+$cartehi-37, 30, 30, "PNG");
			$pdf->SetXY($posX,$posY+19);
			$pdf->SetTextColor(150,150,150);
			$pdf->SetFont('Arial','',7);
			$pdf->Cell($carteli, 3, $siteweb, 0, 0, "C", false, $siteweb);
			
			$colonne++;
			if($colonne==$nbrecol){
				$colonne=0;$ligne++;
				if($ligne==$nbrelig){
					$ligne=0;
					$page++;
					$pdf->AddPage($orientationi, $format);
					$dos = "";
				}
			}
		}
		unset($dos);
	}
}

// S'il reste des cartes sans dos, créer la page finale

if(isset($dos)){
	$pdf->AddPage($orientationi, $format);
	$colonne=0; $ligne=0; 
	foreach($dos as $t){
		$posX = $margel+(($nbrecol-1-$colonne)*$carteli);
		$posY = $margeh+($ligne*$cartehi);
		$pdf->SetXY($posX,$posY+8);
		$pdf->SetTextColor(200,200,200);
		$scdefaut = 30;
		$pdf->SetFont('Arial','B',$scdefaut);
		$largeursc = $pdf->GetStringWidth($scenario);
		if($largeursc>40){$taillesc = round($scdefaut*40/$largeursc, 1);}
		else{$taillesc = $scdefaut;}
		$pdf->SetFont('Arial','B',$taillesc);
		$pdf->Cell($carteli, 8, $scenario, 0, 0, "C", False);
		$pdf->Image($logo, $posX+10 ,$posY+$cartehi-37, 30, 30, "PNG");
		$pdf->SetXY($posX,$posY+19);
		$pdf->SetTextColor(150,150,150);
		$pdf->SetFont('Arial','',7);
		$pdf->Cell($carteli, 3, $siteweb, 0, 0, "C", false, $siteweb);
		// gestion des colonnes, lignes et pages
		$colonne++;
		if($colonne==$nbrecol){$colonne=0;$ligne++;}
	}

}

$pdf->Output();

?>